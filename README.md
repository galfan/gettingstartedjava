# Welcome To Getting Started with Java
> All information in this repo you can found it [here] (https://docs.oracle.com/javase/tutorial/java/)

*i supposed that your environment has been installed*

## single java file should run without IDE(netbeans, eclipse, etc.) just with these commmands:
```bash
javac FirstProgram.java // this sentence create a .class file
```
```bash
//after above sentece run the next sentence
java FirstProgram
```

## the next exercises whitin folder works just with IDE(netbeans/eclipse)


> some files did not upload them to the repo, if some example doesnt works, please notify me.

the gitignore file is a copy of [this] (https://github.com/github/gitignore/blob/master/Java.gitignore)